import { Row, Col, Card} from 'react-bootstrap'


export default function Highlights() {
	return(

		<Row className="HL d-flex justify-content-center p-4">
			<h1 className="d-flex justify-content-center">HOT DEALS</h1>
			<Col xs={12} md={4}>
				<Card className= "mt-2" style={{height: '20rem'}}>
					<Card.Body>
						<Card.Title>
							<h2>ADD HOT ITEM 2</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus eligendi debitis veniam obcaecati, rerum laudantium sint assumenda, nisi, itaque recusandae quasi ipsa harum possimus rem. Blanditiis, corporis, pariatur. Qui, incidunt?
						</Card.Text>
					</Card.Body>
				</Card>	
			</Col>	

			<Col xs={12} md={4}>
				<Card className= "mt-2" style={{height: '20rem'}}>
					<Card.Body>
						<Card.Title>
							<h2>ADD HOT ITEM 2</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus eligendi debitis veniam obcaecati, rerum laudantium sint assumenda, nisi, itaque recusandae quasi ipsa harum possimus rem. Blanditiis, corporis, pariatur. Qui, incidunt?
						</Card.Text>
					</Card.Body>
				</Card>	
			</Col>	

			<Col xs={12} md={4}>
				<Card className= "mt-2" style={{height: '20rem'}}>
					<Card.Body>
						<Card.Title>
							<h2>ADD HOT ITEM 3</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus eligendi debitis veniam obcaecati, rerum laudantium sint assumenda, nisi, itaque recusandae quasi ipsa harum possimus rem. Blanditiis, corporis, pariatur. Qui, incidunt?
						</Card.Text>
					</Card.Body>
				</Card>	
			</Col>	
		</Row>
	)
}