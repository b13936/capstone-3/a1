import { Button, Row, Col } from 'react-bootstrap'

export default function Banner() {
	return(
		<Row className= "banner p-2">
			<Col className= "bannerCol p-5 d-flex align-items-end justify-content-start">
				<div>
					<h1>Add Store Title</h1>
					<p className="d-flex">
						Add sick store line
					</p>
					<div className="d-flex">
						<Button className="btn" variant="warning">
							SHOP NOW
						</Button>
					</div>
				</div>
			</Col>
		</Row>
	)
}