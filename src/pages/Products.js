import { Fragment, useEffect, useState } from 'react'
import productsData from '../data/productsData'
import ProductCard from './../components/ProductCard'

export default function Products(){

	const [products, setProducts] = useState([]);


	useEffect(() => {
		fetch('http://localhost:4000/api/products')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return (
					<ProductCard key = {product._id} productProp ={product}/>
				)

			}));
		});

	},[]);


	return (
		<Fragment>
			<div className="productHeader">
				<h1>Products</h1>
			</div>
			{products}
		</Fragment>
	)
}